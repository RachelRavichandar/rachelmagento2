<?php
namespace Custom\Notices\Model\Config\Source;

class Color implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
            return array( [
                'value' => 'white',
                'label' => 'white',
            ],
            [
                'value' => 'black',
                'label' => 'black',
            ],
            [
                'value' => 'grey',
                'label' => 'grey',
            ],
            [
                'value' => 'blue',
                'label' => 'blue',
            ],
            [
                'value' => 'green',
                'label' => 'green',
            ],
            [
                'value' => 'turquoise',
                'label' => 'turquoise',
            ],
            [
                'value' => 'purple',
                'label' => 'purple',
            ],
            [
                'value' => 'red',
                'label' => 'red',
            ],
            [
                'value' => 'orange',
                'label' => 'orange',
            ],
            [
                'value' => 'purple',
                'label' => 'yellow',
            ]);
    }
}