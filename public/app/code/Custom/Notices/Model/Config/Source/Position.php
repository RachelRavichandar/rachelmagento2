<?php
namespace Custom\Notices\Model\Config\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
            return array( [
                'value' => 'top-left',
                'label' => 'top-left',
            ],
            [
                'value' => 'top-right',
                'label' => 'top-right',
            ],
            [
                'value' => 'bottom-left',
                'label' => 'bottom-left',
            ],
            [
                'value' => 'bottom-right',
                'label' => 'bottom-right',
            ]);
    }
}