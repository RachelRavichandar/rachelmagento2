<?php
namespace Custom\Notices\Model\Config\Source;

class Style implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
            return array( [
                'value' => 'sticky',
                'label' => 'sticky',
            ],
            [
                'value' => 'shadow',
                'label' => 'shadow',
            ]);
    }
}