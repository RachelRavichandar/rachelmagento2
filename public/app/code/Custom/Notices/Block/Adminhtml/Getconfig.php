<?php
namespace Custom\Notices\Block\Adminhtml;
class Getconfig extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Custom\Notices\Helper\Data $helper,
        array $data = [])
    {
        parent::__construct($context);
        $this->helper = $helper;
    }

    public function getDisplayText()
    {
        return $this->helper->getGeneralConfig('display_text');
    }

    public function getEnable()
    {
        return $this->helper->getGeneralConfig('enable_notices',0);
    }

    public function getCssClasses()
    {
        return $this->helper->getGeneralConfig('display_css_classes',0);
    }

    public function getCssColor()
    {
        return $this->helper->getGeneralConfig('display_color',0);
    }

    public function getCssPosition()
    {
        return $this->helper->getGeneralConfig('display_position',0);
    }

    public function getCssStyle()
    {
        return $this->helper->getGeneralConfig('display_style',0);
    }
}