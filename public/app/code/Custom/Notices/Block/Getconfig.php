<?php
namespace Custom\Notices\Block;
class Getconfig extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Custom\Notices\Helper\Data $helper,
        array $data = [])
    {
        parent::__construct($context);
        $this->helper = $helper;
    }

    public function getDisplayText()
    {
        return $this->helper->getGeneralConfig('display_text');
    }

    public function getEnable()
    {
        return $this->helper->getGeneralConfig('enable_notices');
    }

    public function getCssClasses()
    {
        return $this->helper->getGeneralConfig('display_css_classes');
    }

    public function getCssColor()
    {
        return $this->helper->getGeneralConfig('display_color');
    }

    public function getCssPosition()
    {
        return $this->helper->getGeneralConfig('display_position');
    }

    public function getCssStyle()
    {
        return $this->helper->getGeneralConfig('display_style');
    }
}